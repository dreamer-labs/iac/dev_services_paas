# Deploy Ceph-CSI on Kubernetes

Export your vault credentials as an environment variable

`export VAULT_TOKEN=<VAULT_TOKEN>`

Ensure variables are correct in `deploy_ceph_k8s.yaml`

Ensure vault is up and unlocked. See documentation in `https://gitlab.com/dreamer-labs/iac/dev_services/vault`

`ansible-playbook deploy_ceph_k8s.yaml`
