###

cd tests
Add variables
run ./run.sh

then run
kubectl patch storageclass csi-rbd-sc -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'

## RBD

1. Create Pool

```
ceph osd pool create <POOL NAME>
rbd pool init <POOL NAME>
```

2. Create User

```
ceph auth get-or-create client.kubernetes mon 'profile rbd' osd 'profile rbd pool=<POOL NAME>' mgr 'profile rbd pool=<POOL NAME>'
```

3. Run deployment

```
cd tests
# Add variables to playbook.yml
./run.sh
```

## CephFS

1. Create CephFS Pool and CephFS Metadata Pool

```
ceph osd pool create cepfs_data 4
ceph osd pool create cephfs_metadata 4
```

2. Create CephFS FS

```
ceph fs new cephfs cephfs_metadata cephfs_data
```

3. Run deployment

```
cd tests
# Add variables to playbook.yml
./run.sh
```

Note: The ceph_k8s_admin_id variable *cannot* be in the format `client.admin`. You must leave the `client` piece off.

4. Create cephfs PVC 

```
kubectl apply -f cephfs-pvc.yaml
```


To retrieve the admin key use the following:

```
ceph auth ls
```
