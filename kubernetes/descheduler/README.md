# Kubernetes Descheduler

## The problem

When a kubernetes node is drained and pods get migrated off of the node, kubernetes will not automatically move those workloads back to their original location. This can be problematic when anti-affinity rules are in place to keep replica pods spread out across kubernetes nodes for fault tolerance.

Kubernetes Descheduler can be run as a Job (or CronJob) in the cluster to redistribute workloads across the cluster. 

## How to run

```
kubectl create -f descheduler/kubernetes/rbac.yaml
kubectl create -f configmap.yaml
kubectl create -f descheduler/kubernetes/job.yaml
