# ceph-cluster

A quick and dirty ceph cluster using ceph-deploy on openstack. Works in a pinch for testing.

These instructions were cobbled together from the following sources:

* [https://blog.ndk.name/deploy-ceph-storage-cluster-on-ubuntu-server/]
* [https://github.com/ceph/ceph-deploy]

## Prerequisites

### Quickstart bootstrapping from pre-built container (recommended)
The instructions below will help you to prepare your local workstation for the rancher deployment, however we have provided a docker-compose container with all the required pre-requisites:
```
git clone https://gitlab.com/dreamer-labs/iac/dev_services.git
cd dev_services/deploy-tools
docker-compose build
docker-compose up -d
docker-compose run nawt /bin/bash
```

### Manual installation
The latest versions of `openstackclient` require python3, while I experienced failures running `ceph-deploy` in python3, therefore these instructions are going to require both versions of python. I highly recommend that you build it within a virtual environment or separate docker containers.

> These instructions assume CentOS 7. CentOS 8 is not supported.

```
pip3 install openstackclient ansible
pip2 install ceph-deploy
```

The image and network IDs used in this document are specific to our WAW1 region. The following variables can be exported to overwrite the defaults:

## Deploy Ceph
```
BASE_NAME=ceph-dev
IMAGE_ID=2429d1c9-ff5f-4943-a663-38170191b1b2
EX_NET_ID=6c928965-47ea-463f-acc8-6d4a152e9745
IN_NET_ID=8ba4f9aa-032a-4a33-83f5-d9bea5a03218
SSH_KEY=gitlab
FLAVOR=s1-4
VOLUME_SIZE=50
```

Source a valid `openrc` openstack resource file from your provider and deploy the servers.
The `build_openstack.sh` script will create a 3-node cluster and attach raw block devices as `/dev/sdb`.

The script will create the following two files:
* inventory.ini -- The ansible inventory file needed to prepare the hosts
* hosts         -- A hosts file entry for the nodes, which will be copied to the nodes via ansible.

## INSTALL

```
git clone https://gitlab.com/dreamer-labs/iac/dev_services_paas.git
cd dev_services_paas/ceph
./build_openstack.sh
```

Follow the onscreen prompts to complete installation.

See `build_openstack.sh` and `configure_nodes.yml` for detailed steps.