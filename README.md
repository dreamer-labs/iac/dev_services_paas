# dev_services_paas

This repository contains the code necessary to build a functional kubernetes cluster and external rancher cluster with optional external ceph storage.


## overview

* A 3-node kubernetes cluster built using rke.
* A single node rancher cluster used to manage/monitor the primary cluster.
* An optional ceph storage backend, external to the cluster.

> We used an external ceph storage because it more closely mirrors what we have available in production. This documentation also includes an optional method of running ceph within the kubernetes cluster in a hyper-converged configuration.

## quickstart

We have provided a docker-compose environment that contains all the tools needed to bootstrap a functional cluster.

To use the bootstrapping deploy-tools, you must first [install docker](https://docs.docker.com/get-docker/) and [docker-compose](https://docs.docker.com/compose/install/) onto the workstation where you intend to orchestrate your deployment.

> In Proxmox you can build an instance based on image `9005 ubuntu18.04-docker19.03.8` and docker and docker-compose will be pre-installed for you.

Once that is complete, bring up the environment and connect to the nawt container:

Before you create the container, make sure to create an ssh key that you can use to connect to the individual vms. I prefer to do this *outside* of the container and then mount the volume inside the container:
```
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
eval $(ssh-agent)
ssh-add ~/.ssh/id_rsa
```

Now we are ready to build our nawt container. This is the conainer where we will run most of our commands:

```
cd
git clone https://gitlab.com/dreamer-labs/iac/dev_services.git
git clone https://gitlab.com/dreamer-labs/iac/dev_services_paas.git
cd dev_services/deploy-tools
docker-compose build
docker-compose up -d
docker-compose run -p 127.0.0.1:8200:8200 -v $HOME/dev_services_paas/:/dev_services_paas -v $HOME/../:/dev-services -v $HOME/.ssh/:$HOME/.ssh nawt /bin/bash

# Now we are connected to the container, unseal the vault:
vault-auto-unseal.sh
# Take note of the Initial Root Token and use that for the vault login
vault login <Initial Root Token>
# The token file is stored in ~/.vault-token. Let's also export it as a variable.
export VAULT_TOKEN=$(cat $HOME/.vault-token)
```

From within the nawt container you have access to all required services, including a hashicorp vault containing all the secrets required to build our cluster and a running docker registry.

> Throughout most of this documentation it will be assumed that you are executing commands from within this nawt container. Additional resources are provided in the event that you wish to conduct this deploy another way, but it is assumed that the nawt container is the only supported bootstrapping method.

## Install kubernetes

See [rancher/docs/INSTALL.md](rancher/docs/INSTALL.md) for information on building your cluster.

## Configure proxmox cloud-init

By default, proxmox sets a cloud-init parameter called `package_ugprade` to true. This can cause kernel panics if a VM is rebooted unexpectedly in the middle of an upgrade. We have experienced this issue on CentOS7. If using proxmox, we recommend having your proxmox administrator apply the script changes from the [proxmox/](proxmox/) directory.

## Deploy your applications!

At the conclusion of this documentation you should have a fully-functional ceph-backed kubernetes cluster managed by rancher. See [https://gitlab.com/dreamer-labs/iac/dev_services/](https://gitlab.com/dreamer-labs/iac/dev_services/-/blob/master/README.md) for detailed information on deploying applications to your new cluster.

