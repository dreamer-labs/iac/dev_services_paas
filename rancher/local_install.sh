#!/usr/bin/env bash
PLATFORM=amd64
DOCTL_VERSION=1.36.0
KUBECTL_VERSION=1.16.2
RKE_VERSION=1.0.4
HELM_VERSION=3.0.3
RANCHER_VERSION=2.3.
GOLANG_VERSION=1.13.7
MINIO_VERSION=2020-01-25T03-02-19Z

if [ "$EUID" -ne 0 ]; then
  echo "Please run with sudo or as root."
  exit
fi

if ! hash doctl 2> /dev/null; then
  echo "Installing doctl ${DOCTL_VERSION}"
  curl -s -LO https://github.com/digitalocean/doctl/releases/download/v${DOCTL_VERSION}/doctl-${DOCTL_VERSION}-linux-${PLATFORM}.tar.gz
  tar xzf doctl-${DOCTL_VERSION}-linux-${PLATFORM}.tar.gz
  chmod +x doctl
  mv doctl /usr/local/bin/doctl
  rm doctl-${DOCTL_VERSION}-linux-${PLATFORM}.tar.gz
  doctl auth init
else
  if [[ $(doctl version | head -1) =~ ${DOCTL_VERSION} ]] ; then
    echo "INFO: doctl requirement ${DOCTL_VERSION} satisfied."
  else
    echo "WARNING: $(doctl version | head -1), expected ${DOCTL_VERSION}."
  fi
fi

if ! hash kubectl 2> /dev/null; then
  echo "Installing kubectl ${KUBECTL_VERSION}"
  curl -s -L -o /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/${PLATFORM}/kubectl
  chmod +x /usr/local/bin/kubectl

  if [[ ! -d /etc/bash_completion.d/ ]] ; then
    mkdir -p /etc/bash_completion.d/
  fi
  kubectl completion bash | tee -a /etc/bash_completion.d/kubectl > /dev/null

  if ! type _init_completion 2&>1 /dev/null ; then
    grep -qxF 'source /usr/share/bash-completion/bash_completion' ${HOME}/.bashrc || echo 'source /usr/share/bash-completion/bash_completion' >> ${HOME}/.bashrc
  fi

  grep -qxF 'source <(kubectl completion bash)' ${HOME}/.bashrc || echo 'source <(kubectl completion bash)' >> ${HOME}/.bashrc
  . ${HOME}/.bashrc
else
  if [[ $(kubectl version --client --short) =~ ${KUBECTL_VERSION} ]] ; then
    echo "INFO: kubectl requirement ${KUBECTL_VERSION} satisfied."
  else
    echo "WARNING: kubectl $(kubectl version --client --short), expected ${KUBECTL_VERSION}."
  fi
fi

if ! hash rke 2> /dev/null; then
  echo "Installing rke ${RKE_VERSION}"
  curl -s -L -o /usr/local/bin/rke https://github.com/rancher/rke/releases/download/v${RKE_VERSION}/rke_linux-${PLATFORM}
  chmod +x /usr/local/bin/rke
else
  if [[ $(rke --version) =~ ${RKE_VERSION} ]] ; then
    echo "INFO: rke requirement ${RKE_VERSION} satisfied."
  else
    echo "WARNING: $(rke --version), expected ${RKE_VERSION}."
  fi
fi

if ! hash helm 2> /dev/null; then
  echo "Installing helm ${HELM_VERSION}"
  curl -s -L -o helm-v${HELM_VERSION}-linux-${PLATFORM}.tar.gz https://get.helm.sh/helm-v${HELM_VERSION}-linux-${PLATFORM}.tar.gz
  tar xzf helm-v${HELM_VERSION}-linux-${PLATFORM}.tar.gz
  chmod +x linux-${PLATFORM}/helm
  mv linux-${PLATFORM}/helm /usr/local/bin/helm
  rm helm-v${HELM_VERSION}-linux-${PLATFORM}.tar.gz
  rm -rf linux-${PLATFORM}/
else
  if [[ $(helm version --short) =~ ${HELM_VERSION} ]] ; then
    echo "INFO: helm requirement ${HELM_VERSION} satisfied."
  else
    echo "WARNING: helm version $(helm version --short), expected ${HELM_VERSION}."
  fi
fi

if ! hash rancher 2> /dev/null; then
  echo "Installing rancher cli ${RANCHER_VERSION}"
  curl -s -L -o rancher-linux-${PLATFORM}-v${RANCHER_VERSION}.tar.gz https://releases.rancher.com/cli2/v${RANCHER_VERSION}/rancher-linux-${PLATFORM}-v${RANCHER_VERSION}.tar.gz
  tar xzf rancher-linux-${PLATFORM}-v2.3.2.tar.gz
  chmod +x rancher-v${RANCHER_VERSION}/rancher
  mv rancher-v${RANCHER_VERSION}/rancher /usr/local/bin/rancher
  rm rancher-linux-${PLATFORM}-v${RANCHER_VERSION}.tar.gz
  rm -rf rancher-v${RANCHER_VERSION}/
else
  if [[ $(rancher --version) =~ ${RANCHER_VERSION} ]] ; then
    echo "INFO: rancher requirement ${RANCHER_VERSION} satisfied."
  else
    echo "WARNING: $(rancher --version), expected ${RANCHER_VERSION}."
  fi
fi

if ! hash mc 2> /dev/null; then
  echo "Installing minio cli ${MINIO_VERSION}"
  curl -s -L -o /usr/local/bin/mc https://dl.min.io/client/mc/release/linux-${PLATFORM}/archive/mc.RELEASE.${MINIO_VERSION}
  chmod +x /usr/local/bin/mc
else
  if [[ $(mc --version) =~ ${MINIO_VERSION} ]] ; then
    echo "INFO: mc requirement ${MINIO_VERSION} satisfied."
  else
    echo "WARNING: $(mc --version), expected ${MINIO_VERSION}."
  fi
fi

if ! hash go 2> /dev/null; then
  echo "Installing golang ${GOLANG_VERSION}"
  curl -s -LO https://dl.google.com/go/go{$GOLANG_VERSION}.linux-${PLATFORM}.tar.gz
  tar -C /usr/local -xzf go${GOLANG_VERSION}.linux-${PLATFORM}.tar.gz
  rm -f go${GOLANG_VERSION}.linux-${PLATFORM}.tar.gz
  grep -qxF 'export PATH=$PATH:/usr/local/go/bin' ${HOME}/.bashrc || echo 'export PATH=$PATH:/usr/local/go/bin' >> ${HOME}/.bashrc
  grep -qxF 'export GOROOT=/usr/local/go' ${HOME}/.bashrc || echo 'export GOROOT=/usr/local/go' >> ${HOME}/.bashrc
  . $HOME/.bashrc
else
  if [[ $(go version) =~ ${GOLANG_VERSION} ]] ; then
    echo "INFO: go requirement ${GOLANG_VERSION} satisfied."
  else
    echo "WARNING: $(go version), expected ${GOLANG_VERSION}."
  fi
fi
