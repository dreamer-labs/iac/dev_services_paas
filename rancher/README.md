# README

This project is intended for demonstration purposes. It will build a 3-node kubernetes cluster and an addition single-node rancher cluster using `rke` and `helm`. The nodes can be Ubuntu 18.04 (preferred) or CentOS 7 virtual machines.  DigitalOcean, OpenStack, and Proxmox are supported platforms

## Quickstart (recommended)

The instructions below will help you to prepare your local workstation for the rancher deployment, however we have provided a docker-compose container with all the required pre-requisites. It is highly recommended that you conduct all of your deployments from within this container, see [INSTALL.md](../INSTALL.md) for details on setting up your container.

## Local bootstrapping (not-recommended)

If for any reason you feel it is necessary to configure your local workstation to conduct the deployment, instead of using the provided docker-compose environment, you can do so:

The following will install `python3` and `python3-pip` system-wide and set them as default. You may prefer to install them into a virtual environment. That is left as an exercise to the user.

It is highly recommended that you install ansible via pip, rather than from your distribution's package manager.
As of this writing, the ansible version in pypi is `2.9.4`, while the version in the ubuntu 18.04 repository is at `2.5.1`. Your mileage may vary.

### Ubuntu/Debian

```
sudo apt update
sudo apt install -y openssl curl python3 python3-pip bash-completion
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 10
sudo update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 10
sudo pip install ansible
```

### RHEL/CentOS

```
sudo yum install -y openssl curl python3 python3-pip bash-completion
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 10
sudo update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 10
sudo pip install ansible
```

## Prerequisites

Everything in this repository is designed to be run from your local workstation or virtual machine, so you will need to install the following utilities before you begin. The following versions have been tested:

```
ansible     = 2.9.4
openssl     = 1.1.1
curl        = 7.58.0
python3     = 3.6.9
python3-pip = 9.0.1
doctl       = 1.36.0
kubectl     = 1.16.2
rke         = 1.0.4
helm        = 3.0.3
rancher     = 2.3.2
mc          = 2020-01-25
```

We have provided simple distribution-agnostic script to install the aforementioned requirements:

```
./local_install.sh
```

> NOTE: The script provided is expected to be run with super user (`sudo`) permissions. 

## Installation

### See [docs/INSTALL.md](docs/INSTALL.md) for cluster building instructions.

## Node requirements

As of this writing, the following Linux distributions and Docker versions are supported.

* CentOS 7.5 / 7.6 / 7.7
* Ubuntu 18.04
* Docker 17.03.2 / 18.06.2 / 18.09.x / 19.03.x

*CentOS 8 is not yet supported.*
