#!/usr/bin/env bash

# Verify that doctl is configured for use.
doctl account get 2>&1 /dev/null
if [[ $? != 0 ]]; then
  echo "This script relies on the digitalocean cli (doctl)"
  echo "Please install and configure doctl."
  echo "https://github.com/digitalocean/doctl#installing-doctl"
  exit 1
fi

# Check that the user has exported an SSH key fingerprint.
if [ -z ${DO_SSH_KEY+x} ]; then
  echo "Please set the environment variable DO_SSH_KEY to a public"
  echo "key fingerprint that has been uploaded to DigitalOcean."
  echo "https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/to-account/"
  exit 1
fi

# Ensure that there are no existing nodes tagged for our cluster.
if [[ $(doctl compute droplet list --format "PublicIPv4" --no-header --tag-name kubernetes_cluster | wc -l) -gt 0 ]] ; then
  echo "Existing nodes found with the tag kubernetes_cluster."
  echo "Delete them before continuing"
  exit 1
fi

for node in kube-master kube-n01 kube-n02 kube-n03; do
	doctl compute droplet create ${node} \
		--size s-2vcpu-4gb \
		--image centos-7-x64 \
		--region nyc1 \
		--ssh-keys ${DO_SSH_KEY} \
		--enable-private-networking \
		--tag-name "kubernetes_cluster" \
		--wait
done

# Update the DNS record for kube.homeskillet.org
CONTROL_PLANE=$(doctl compute droplet list --format "PublicIPv4" --no-header --tag-name kubernetes_cluster | tail -1)
doctl compute domain records update homeskillet.org --record-id=89006212 --record-data="${CONTROL_PLANE}"

# Create an ansible inventory file with our new servers.
cat > inventory.ini <<EOL
[nodes]
$(doctl compute droplet list --format "PublicIPv4" --no-header --tag-name kubernetes_cluster)

[nodes:vars]
ansible_connection=ssh
ansible_ssh_user=root
EOL
