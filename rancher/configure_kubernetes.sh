#!/usr/bin/env bash


CLUSTER_NAME="PRODUCTION"

echo "Checking that we have exactly 4 nodes for our cluster..."
IP_ADDRESSES=$(doctl compute droplet list --format "PublicIPv4" --no-header --tag-name kubernetes_cluster)
if [[ $(echo "$IP_ADDRESSES" | wc -l) -ne 4 ]] ; then
  "There are not exactly four nodes tagged for this cluster. exiting."
  exit 1
fi

PUBLIC_IPS=$(doctl compute droplet list --tag-name kubernetes_cluster --no-header --format "PublicIPv4" | tac)

PRIVATE_IPS=$(doctl compute droplet list --tag-name kubernetes_cluster --no-header --format "PrivateIPv4" | tac)

cp cluster_template.yml cluster_${CLUSTER_NAME}.yml

for ip in ${PUBLIC_IPS}; do
  sed -i -e "0,/{{public_ip}}/ s/{{public_ip}}/${ip}/" cluster_${CLUSTER_NAME}.yml
done

for ip in ${PRIVATE_IPS}; do
  sed -i -e "0,/{{private_ip}}/ s/{{private_ip}}/${ip}/" cluster_${CLUSTER_NAME}.yml
done
