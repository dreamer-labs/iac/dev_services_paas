## Hosting Providers

The included scripts are designed to build your rancher cluster on DigitalOcean, ovh.net OpenStack or Proxmox.

### DigitalOcean

Generate a DigitalOcean API token:

* Navigate in the DO portal to `Manage \ API \ Tokens/Keys`.
* Grant the token both read and write access.
* export your new token in your `~/.bashrc` file as `DO_API_KEY`.

Upload your SSH public key to DigitalOcean:

* Navigate in the DO portal to `Account \ Security`.
* Export the fingerprint of this newly added key in your `~/.bashrc` file as `DO_SSH_KEY`.

This key will be used for ssh access to each of your nodes, and for ansible provisioning.

Run the following script to create the virtual machines and create your cluster definition file and ansible inventory files.

```
./build_digitalocean.sh
```

### Openstack

You will need to retrieve the following from your openstack provider:

* An [openrc](https://horizon.cloud.ovh.net/project/api_access/openrc/) file used for authentication. [Link](https://docs.ovh.com/gb/en/public-cloud/set-openstack-environment-variables/)
* A valid SSH [public key](https://horizon.cloud.ovh.net/project/key_pairs) must be uploaded into the proper OVH region. [Link](https://docs.ovh.com/gb/en/dedicated/ovh-ssh-key/)

The following rancher-specific environment variables can be exported to overwrite the defaults, based on your needs. Please ensure that you are using valid values for your openstack environment/region:

```
CLUSTER_NAME= # Default: PRODUCTION
              #          Can be set to whatever you like.
BASE_NAME=    # Default: kube
              #          Can be set to whatever you like.
IMAGE_ID=     # Must be a valid CentOS 7 image in your OVH region.
EX_NET_ID=    # Must be a valid External Network ID in your OVH region.
IN_NET_ID=    # Must be a valid Private Network ID in your OVH region.
SSH_KEY=      # Must be a valid name of an SSH public key in your OVH region.
FLAVOR=       # Default: s1-8
              #          Must be a valid flavor (size) for instances in your OVH region.
VOLUME_SIZE=  # Default: 50
                         Volume size, in gigabytes, to attach to your instances.
```

Running build_openstack.sh will check variables with no default value.  In the event that these environmental variables are not set, build_openstack.sh will prompt for a value.

Source your openrc file to authenticate with openstack.

Run the following script to create the virtual machines and create your cluster definition file and ansible inventory files.

```
./build_openstack.sh
```

### Proxmox

We do not presently have an automated system for provisioning in Proxmox, but it is in the works.

In the meantime:

* Run `./build_proxmox.sh` to create the ansible inventory and rke cluster definition files.
* Make four full clones of `Virtual Machine 9005 (ubuntu18.04-docker19.03.8) on node proxmox-01`named as follows:
  - kube-01
  - kube-02
  - kube-03
  - rancher
* Distribute/migrate the 3 kube nodes to different proxmox nodes.
* Set the following hardware parameters for each node:

```
Memory: 32768MB (32GB)
Processor: 1 socket, 16 cores
Hard disk SCSI0, resize to 50GB
  - Target Storage: vms
Hard disk SCSI1, resize to 25GB
  - Target Storage: vms
Network Device net0:
  - type: virtio
  - bridge: vmbr2
  - vlan: 3005
  - firewall: disabled
Network Device net1:
  - type: virtio
  - bridge: vmbr2
  - vlan 2005
  - firewall: disabled
```

* We must also configure settings for the new vms in the cloud-init section of proxmox:

```
User: ubuntu
Password: optional if you want to login from the proxmox console
SSH Public Key: upload the public key from the server where you will conduct the deploy (most-likely the nawt container).
IP Config (net0):
  - ip=172.20.5.101/24
IP Config (net1):
  - ip=10.20.5.101/24
  - gw=10.20.5.254
```

> We have reserved ips `101, 102, and 103` for our kubectl nodes and `105` for our rancher node. Be sure to set the IP Config accordingly for all nodes or manually modify the `inventory.ini` and `cluster_DEV.yml` and `cluster_RANCHER.yml`!!!

## Return to [INSTALL.md](INSTALL.md)