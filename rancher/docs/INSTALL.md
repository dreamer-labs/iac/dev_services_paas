# INSTALL.md

## Build cluster nodes

See [PROVIDERS.md](PROVIDERS.md) for detailed information on how to provision the required virtualmachine infrastructure for your cloud provider, along with the required ansible `inventory.ini` and `rke` cluster configuration files.


## DNS
Rancher really likes to have a qualified domain name. Set your DNS record for somewhere, and export it as an environment variable `RANCHER_DOMAIN`. We will use that variable in later steps. Configure the DNS record to point to the IP of your rancher node (10.20.5.105 if you are running on Proxmox).
```
export RANCHER_DOMAIN=rancher.dev.dldev.xyz
```

## Install docker on all nodes via ansible

> NOTE: Skip this step if building from the proxmox base 9005 image ubuntu18.04-docker19.03.8 or are performaing an air-gapped installation. Docker is already pre-installed on that image.

> OpenStack: I have noticed that because of the unreliable manner in which IP addresses are returned from openstack, in some regions the inventory.ini is populated with IPv6 addresses. I think I have fixed this in recent updates, but be sure to sanity check your inventory.ini and cluster configuration files...

### RHEL7/CentOS7 Target hosts:
```
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i inventory.ini install_docker_cent.yml
```

### Ubuntu 18.04:
```
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i inventory.ini install_docker_ubuntu.yml
```

## Build the cluster with RKE and install rancher
> NOTE: the rke configuration was plagued by the same IPv6 issue as inventory.ini, so just make sure to sanity check and update the public IP addresses if necessary.

```
rke up --config cluster_DEV.yml
```

> NOTE: If this command fails complaining that it couldn't wait for a process to finish, just run the command again. This only seems to happen on Ubuntu 18.04 and we haven't had a chance to troubleshoot it yet.

This will generate 2 files in the local directory:
* kube_config_cluster_DEV.yml: The Kubeconfig file for the cluster, this file contains credentials for full access to the cluster.
* cluster_DEV.rkestate: The Kubernetes Cluster State file, this file contains credentials for full access to the cluster.

Connect to your cluster with `kubectl` by exporting the `$KUBECONFIG` variable:
```
export KUBECONFIG=$(pwd)/kube_config_cluster_DEV.yml
kubectl get nodes
```

Verify that your cluster is responsive with `kubectl get nodes` and `kubectl cluster-info`. 

## Build a stand-alone rancher cluster
We want rancher to run in a separate cluster. This is what our Proxmox server on IP `10.20.5.105` is for. If you are using a different IP address, update `cluster_RANCHER.yml`

```
rke up --config cluster_RANCHER.yml
```

Connect to your new rancher cluster with `kubectl` by exporting the `$KUBECONFIG` variable:
```
export KUBECONFIG=$(pwd)/kube_config_cluster_RANCHER.yml
kubectl get nodes
```

## Add the rancher repository
```
helm repo add rancher-latest https://releases.rancher.com/server-charts/latest
helm repo update
```

## Create the cattle-system namespace
```
kubectl create namespace cattle-system
```

## Install rancher
We will install rancher and set `ingress.tls.source=secret` so that we can provide our own TLS certificates.
```
helm install rancher rancher-latest/rancher \
  --namespace cattle-system \
  --set hostname=rancher.dev.dldev.xyz \
  --set ingress.tls.source=secret \
  --set privateCA=true \
  --set useBundledSystemChart=true
```
## TLS
We have two options here, we can generate our own brand new CA certificate or use the one stored in our vault.

### Generate a TLS certificate
Generate a TLS certificate for our domain. Modify the domain name and IP to point to your rancher instance.
```
docker run -v $PWD/certs:/certs \
  -e SSL_SUBJECT=rancher.dev.dldev.xyz \
  -e SSL_DNS=rancher.dev.dldev.xyz \
  -e SSL_IP=10.20.5.105 \
  superseb/omgwtfssl

cp certs/ca.pem certs/cacerts.pem
```

### Retrieve a certificate from the vault
From our nawt container, we have a vault which contains the certs:
```
cd /dev_services_paas/rancher
mkdir -p certs
vault-auto-unseal
vault login <Initial Root Token>
vault kv get --field key.pem dev/secrets/rancher/certs > certs/key.pem
vault kv get --field cert.pem dev/secrets/rancher/certs > certs/cert.pem
vault kv get --field cacerts.pem dev/secrets/rancher/certs > certs/cacerts.pem
vault kv get --field ca-key.pem dev/secrets/rancher/certs > certs/ca-key.pem
```

### Add the CA certificate as a trusted CA.
Add this to the workstation where you handle your deployments (i.e. the nawt container):
```
cp certs/cacerts.pem /usr/local/share/ca-certificates/rancher-ca.crt
update-ca-certificates
```

### Add the trusted CA to your browser.
This is different depending on your browser, in Firefox go to `Options`, `Security & Privacy`, scroll all the way to the bottom of the page where it says `View certificates` and then click `Import` to import the cacerts.pem file into Firefox so that your browser will trust rancher (and all the apps deployed in rancher).

## Install our certificates into rancher
```
kubectl -n cattle-system create secret tls tls-rancher-ingress \
  --cert=certs/cert.pem \
  --key=certs/key.pem

cp certs/ca.pem certs/cacerts.pem

kubectl -n cattle-system create secret generic tls-ca --from-file=certs/cacerts.pem
```

## Controlling our cluster from rancher

Login to rancher at `https://rancher.dev.dldev.xyz`. If it gives you a 503 error, just be patient! It sometimes takes a few minutes to start up.

### Import an external cluster

* Login to the rancher web interface and use the menu at the top-left to navigate to the global space. 
* Click the "Add Cluster" button and select "Import an existing cluster"
* Give the cluster a name and click "Create"

Rancher will give you a kubectl command which you will run against the cluster you wish for rancher to manage. Be sure to update your `KUBECONFIG` to point back to our DEV cluster, and then run the kubectl commmand.

Rancher will connect to our dev cluster and can now be used to manage it.

### Enable monitoring
Rancher has built-in prometheus monitoring.

* Login to the rancher web interface and use the menu at the top left to navigate to the dev cluster.
* Select the "Tools" dropdown menu and select "Monitoring"

## Attach to existing Ceph storage
* See [kubernetes/README.md](../../kubernetes/README.md) for instructions on using ansible to attach to an existing stand-alone ceph cluster (preferred).
* See [rancher/README.md](../README.md) for instructions on installing a hyper-converged ceph cluster inside of kubernetes.
* See [ceph/README.md](../../ceph/README.md) for instructions on building your own stand-alone ceph cluster.

## Install metallb
See [metallb/README.md](../../kubernetes/metallb/README.md)