#!/usr/bin/env bash

CLUSTER_NAME=${CLUSTER_NAME:-DEV}
BASE_NAME=${BASE_NAME:-kube}

declare -a PUBLIC_IPS=(10.20.5.101 10.20.5.102 10.20.5.103 10.20.5.105)
declare -a PRIVATE_IPS=(172.20.5.101 172.20.5.102 172.20.5.103 172.20.5.105)

echo "Writing inventory.ini"

cat > inventory.ini <<EOL
[nodes]
$(printf '%s\n' "${PUBLIC_IPS[@]}")

[nodes:vars]
ansible_connection=ssh
ansible_ssh_user=ubuntu
ansible_ssh_common_args='-o StrictHostKeyChecking=no'
EOL

cp cluster_template.yml cluster_${CLUSTER_NAME}.yml

echo "Writing cluster_${CLUSTER_NAME}.yml"

for ip in ${PUBLIC_IPS[@]}; do
  sed -i -e "0,/{{public_ip}}/ s/{{public_ip}}/${ip}/" cluster_${CLUSTER_NAME}.yml
done

for ip in ${PRIVATE_IPS[@]}; do
  sed -i -e "0,/{{private_ip}}/ s/{{private_ip}}/${ip}/" cluster_${CLUSTER_NAME}.yml
done

echo "Complete. Remember to update DNS!"

