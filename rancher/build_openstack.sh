#!/usr/bin/env bash

CLUSTER_NAME=${CLUSTER_NAME:-PRODUCTION}
BASE_NAME=${BASE_NAME:-kube}
FLAVOR=${FLAVOR:-s1-8}
VOLUME_SIZE=${VOLUME_SIZE:-50}

# Check for OpenStack specific environment variables
if [ -z ${IMAGE_ID+x} ]; then
  read -p "OpenStack Image ID (CentOS 7 preferred): " IMAGE_ID
fi

if [ -z ${EX_NET_ID+x} ]; then
  read -p "OpenStack external network ID: " EX_NET_ID
fi

if [ -z ${IN_NET_ID+x} ]; then
  read -p "OpenStack internal network ID: " IN_NET_ID
fi

if [ -z ${SSH_KEY+x} ]; then
  read -p "OpenStack SSH key pair name: " SSH_KEY
fi

declare -a PUBLIC_IPS=()
declare -a PRIVATE_IPS=()

for i in 1 2 3 4 ; do
  SERVER_NAME=${BASE_NAME}-0${i}
  echo "Creating ${SERVER_NAME}..."
  VOL_ID=$(openstack volume create -f value -c id --size ${VOLUME_SIZE} --type classic ${BASE_NAME}-vol${i})
  INSTANCE_ID=$(openstack server create -f value -c id --wait \
                          --nic net-id=${EX_NET_ID} \
                          --nic net-id=${IN_NET_ID} \
                          --flavor ${FLAVOR} \
                          --image ${IMAGE_ID} \
                          --key-name ${SSH_KEY} \
                          --block-device-mapping sdb=${VOL_ID} \
                          ${SERVER_NAME})

  OS_ADDRESSES=$(openstack server show ${INSTANCE_ID} -f value -c addresses)
  PUBLIC_IP=$(echo $OS_ADDRESSES | cut -f 2 -d '=' | cut -f 1 -d ',')
  PRIVATE_IP=$(echo $OS_ADDRESSES | cut -f 3 -d '=')
  PUBLIC_IPS+=("${PUBLIC_IP}")
  PRIVATE_IPS+=("$PRIVATE_IP")
done


# Create an ansible inventory file with our new servers.
cat > inventory.ini <<EOL
[nodes]
$(printf '%s\n' "${PUBLIC_IPS[@]}")

[nodes:vars]
ansible_connection=ssh
ansible_ssh_user=centos
ansible_ssh_common_args='-o StrictHostKeyChecking=no'
EOL


cp cluster_template.yml cluster_${CLUSTER_NAME}.yml

echo "Writing cluster_${CLUSTER_NAME}.yml"

for ip in ${PUBLIC_IPS[@]}; do
  sed -i -e "0,/{{public_ip}}/ s/{{public_ip}}/${ip}/" cluster_${CLUSTER_NAME}.yml
done

for ip in ${PRIVATE_IPS[@]}; do
  sed -i -e "0,/{{private_ip}}/ s/{{private_ip}}/${ip}/" cluster_${CLUSTER_NAME}.yml
done


echo "Complete. Remember to update your DNS!"

