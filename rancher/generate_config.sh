#!/usr/bin/env bash

IP_ADDRESSES=$(doctl compute droplet list --tag-name kubernetes_cluster --no-header --format "PublicIPv4")
IP_SPACES=$(echo "$IP_ADDRESSES" | sed -e 's/^/https:\/\//' -e 's/$/:9000\/data\//' | tr '\n' ' ' | sed -e 's/[[:space:]]*$//')
IP_COMMAS=$(echo "$IP_ADDRESSES" | tr -s '\n' ',' | sed -e 's/,*$//')

echo "generating config file etc/default/minio"
# Once we move to production I'll store these KEYS in an ansible vault or something
# but for the purposes of this demo, these should be plenty secure as the
# servers are not intended to be online more than a couple of hours at a time.

cat > ./minio.default << EOF
[Service]
Environment="MINIO_OPTS=${IP_SPACES}"
Environment="MINIO_ACCESS_KEY=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w26 | head -n1)"
Environment="MINIO_SECRET_KEY=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w34 | head -n1)"
EOF

echo "generating TLS certificate for hosts: ${IP_COMMAS}"
go run $GOROOT/src/crypto/tls/generate_cert.go -ca --host "${IP_COMMAS}"

mv key.pem private.key
mv cert.pem public.crt