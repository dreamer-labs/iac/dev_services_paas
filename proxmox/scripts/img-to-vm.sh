#!/bin/bash

DATASTORE="vms"
IMG_PATH=tmp-img-to-template.img

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --image-url)
    IMAGE_URL=$2
    if [[ -z "$IMAGE_URL" ]];
    then
      echo "No --image-url specified"
      exit 1
    fi
    shift # past flag
    shift # past argument
    ;;
    --vm-id)
    VM_ID=$2
    if [[ -z "$VM_ID" ]];
    then
      echo "No --vm-id specified"
      exit 1
    fi
    shift
    shift
    ;;
    --datastore)
    DATASTORE=$2
    if [[ -z "$DATASTORE" ]];
    then
      echo "No --datastore specified"
      exit 1
    fi
    shift
    shift
    ;;
    -h|--help)
    echo -e "Downloads a cloud .img from --image-url and exposes it as a proxmox 9xxx template."
    echo -e "\n\t--image-url: image url to download."
    echo -e "\n\t--vm-id: an image id... usually 9001, 900x etc... make sure it's available."
    exit;
    shift
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters

wget $IMAGE_URL -o $IMG_PATH

qm create $VM_ID --memory 2048 --net0 virtio,bridge=vmbr0

qm importdisk $VM_ID $IMG_PATH $DATASTORE

qm set $VM_ID --scsihw virtio-scsi-pci --scsi0 ${DATASTORE}:vm-${VM_ID}-disk-0

qm set $VM_ID --ide2 $DATASTORE:cloudinit

qm set $VM_ID --boot c --bootdisk scsi0

qm set $VM_ID --serial0 socket --vga serial0

echo "Your instance has been imported and created as $VM_ID"
echo "Run 'qm template $IMAGE_ID' once your are done with pre-config to create a template from your instance"
