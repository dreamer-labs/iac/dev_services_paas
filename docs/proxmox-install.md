# Proxmox Server Installation

For the Development Enclaves we have decided to work with Proxmox for the base Hypervisor for the IaaS. This document will go through the process of configuring Proxmox for the environment.

## Hardware Installation and  Configuration

For this deployment we are going to be using TYAN B8242T76AV26HR-2V servers and Intel 82599ES 10GB SPF NICs. Since we are installing Proxmox which will use CEPH for the primary shared storage, we need to re-configure the internal cabling eliminating the internal RAID controller since it only is attached to 8 of the 26 drives.

After opening the server, remove the riser cards and remove the middle cross brace. You need to disconnect the SFF-8643 cables from port 0 and port 1 on the RAID controller, and connect to J1 and J2 on the motherboard. The RAID controller can be removed or left in place. Re-install the center support, this will be difficult since the connector is directly under the support. Install the Intel NIC in one of the riser cards and re-install to the server.

## Configuring the BMC in the BIOS

To configure the BMC, you will need to boot the computer into the BIOS. By default, the BMC will attempt to use DHCP on the default VLAN to pull an address. If it is successful at pulling an address it will show on the POST screen. If not, you will need to enter the BIOS settings and manually configure.

Start by entering the BIOS by pressing the DEL key during POST.

!NOTE:  Servers will take a long time to boot.  POST process will start after the server manufacturer's splash screen goes away.  Wait for the "American Megatrends, Inc." logo to appear.

![BIOS Setup Screen](images/bios_setup_screen.png)

During this process we also need to disable the hardware alarm as we are only using a single power supply in the dev rack, otherwise it will alarm constantly.

![BIOS Hardware Monitoring](images/bios_hardware_monitoring.png)

The BMC Management configuration can be found under the Server Mgmt tab.

![BIOS Server Management](images/bios_server_mgmt.png)

The BMC can be configured using this screen. You will notice that you don't have the ability to configure a vlan, so you will need to have the network port configured as an access port for the proper vlan. We only need to configure the Management Port 1, don't enable Port 2.

![BIOS BMC Network](images/bios_bmc_network.png)

!NOTE:  "Router MAC address" is not a required field.  Ask network administrator for value(s) to be entered as last octet in "Station IP address" field.

After saving and exiting, the server will reboot and you should have access to the BMC over the network.

## Configuring the BMC with ipmitool

If you already have a debian-based distribution installed on the server (such as Proxmox) you can configure the IPMI interface as follows:

```bash
sudo apt update && sudo apt install -y ipmitool iputils-arping

# Get the last octet of your ip4 address
last_octet=$(ip a s vmbr0 | grep inet[^6] | awk -F '[./]' '{ print $4}')

# Get the mac address of your gateway
gw_ip="10.21.127.254"
gw_mac=$(arping -c 1 -I vmbr0 ${gw_ip} | grep reply | awk -F '[\[\]]' '{ print $2 }')

# Get the vlan id. Currently 2256
vlan_id="2256"

ipmitool lan set 1 ipsrc static
ipmitool lan set 1 ipaddr 10.21.4.${last_octet}
ipmitool lan set 1 netmask 255.255.128.0
ipmitool lan set 1 vlan id ${vlan_id}
ipmitool lan set 1 defgw ipaddr ${gw_ip}
ipmitool lan set 1 defgw macaddr ${gw_mac}

# Check your work and make sure your settings are correct
ipmitool lan print 1
```

> NOTE: You may need to reboot before it accepts the gateway macaddr. Your mileage may vary.

## Configure BMC for Image Redirection

Since in the dev rack we don't have a PXE boot server that is configured to boot an image to (re)install the hosts, we need to mount an ISO image of Proxmox to perform the installation process. Once you have the ability to get access to the BMC, you can configure this using a NFS mount that is configured on the Proxy node.

To access BMC, you will use the address that you configured in the previous steps.

![BMC Login Screen](images/bmc_login_screen.png)

The default credentials are `root`/`superuser`. I have seen at times in Firefox you may need to login a couple times to get logged in successfully.

Once you have logged in you need to add the NFS server in Settings > Media Redirection Settings > General Settings.

![BMC Media Redirection Settings](images/bmc_media_redirection_settings.png)

You will enable Remote Media Support and enable Mount CD/DVD. The server address is the BMC Network address that is assigned to the Proxy server, at this time it is 10.21.127.10, and the current path is `/var/nfsshare`.

``` text
Enable Remote Media Support
Enable Mount CD/DVD
Server address: 10.21.4.100
Path in Server: /var/nfsshare
Share Type for CD/DVD:  nfs
```

Once this has been enabled, you need to mount the ISO. This needs to be done from the main page under Image Redirection > Remote Media. You will select the Proxmox ISO and press the play button to start the redirection.

![BMC Remote Media](images/bmc_remote_media.png)

If you don't receive any errors, this will attach the ISO as a boot device.

## ProxMox Installation

Once you have the media redirection settings configured, we are ready to start the installation process for Proxmox. If you don't currently have an OS installed, after you reboot the server it should automatically boot to the redirected ISO file.

![ProxMox GRUB Screen](images/proxmox_install_grub.png)

This will start the Proxmox Installation, select `Install Proxmox VE` if the install process does not continue after this point.

![ProxMox EULA](images/proxmox_eula.png)

The next screen you will see is the drive selection screen, on this screen we need to configure the main OS Drives. Since the main OS Drives can't be setup on the Tyan servers in a hardware RAID configuration, we need to setup a software RAID 1. In the filesystem dropdown, select `ZFS (RAID1)`. This will configuration the OS Drives in a a RAID 1 configuration using zfs as the base filesystem.

For the drive selection, you only want to select the two smaller OS drives to be placed in the ZFS mirror. They should be 223GB in size. In our example they are the following:

* /dev/sdi
* /dev/sdj

For the rest of the drives, select `--- do not use ---`. They will be used for CEPH later and can't be associated with the main OS set. At this time we don't need to change any of the Advanced ZFS settings.

![ProxMox Drive Options](images/proxmox_drive_options.png)

Next will be location and timezone selection:

![Proxmox Location and Timezone](images/proxmox_location_and_timezone.png)

Next you will set the Admin (root) password and recovery email. Use the Admin password we currently have documented in Google docs. The recovery e-mail doesn't matter since we don't have an e-mail server setup in the B Network. You will have to modify the default as it will see it as an invalid address.

![Proxmox Admin Password](images/proxmox_admin_password.png)

Next you will need to configure the network interface. You can input the needed information here but on reboot it will not work with modification; as you can see we can't input any VLAN configuration. This could either be addressed by setting the default VLAN on the port, or manually editing the network configuration after installation.

**** You will need to input the correct IP Address information depedning on the network you are installing into.**

![Proxmox network Configuration](images/proxmox_network_configuration.png)

Next you will see the Summary screen, if everything looks good, hit next.

![Proxmox Summary Screen](images/proxmox_summary_screen.png)

If everything completes successfully you will see the "Installation Successful" screen. Just reboot the server but keep the console open.

![Proxmox Installation Success](images/proxmox_installation_success.png)

## Proxmox Configuration

Since we are not booting off a PXE installation source, we don't have the ability to configure the VLAN interfaces on the bridge so we have to manually do that after the host reboots.

From the initial boot, the only interface that you need to configure is the management interface.

Once the server reboots, you will be prompted to login to the console.
Login as root, and edit the `/etc/network/interfaces` file with the editor of your choice (VIM is not installed by default, boo).

### Bonded Fiber Configuration

Because we have the possibility of running the on board NICs and the add/on NICs we are doing bonding differently than we have in the past. The Mellonox card has the ability to use up to 25 Gigabit SFPs. These will be used for the "public" network connection. The other bond, which is on the Intel Card, will be used for the inter-rack communication. Depending if we have the Intel SFPs in, you will need to make an update to the initramfs to get these to work.

#### Updating the initramfs

First you need to create a configuration file for the module.

Create `/etc/modprobe.d/ixgbe.conf` with the following line:

``` ini
options ixgbe allow_unsupported_sfp=1
```

After adding that option to the file, update the initramfs:
We are spliting the bonds between the 2 nics, this will need to be reflected in the bonding configuration.  

``` bash
root@proxmox:~# update-initramfs -u
```


Edit `/etc/network/interfaces` to look like the following:

``` ini
auto lo
iface lo inet loopback

auto enp33s0f0
iface enp33s0f0 inet manual

iface enp2s0f3u2u2c2 inet manual

iface enp65s0f0 inet manual

iface enp65s0f1 inet manual

auto enp33s0f1
iface enp33s0f1 inet manual

auto enp1s0f0
iface enp1s0f0 inet manual

auto enp1s0f1
iface enp1s0f1 inet manual

auto bond0
iface bond0 inet manual
        bond-slaves enp1s0f0 enp33s0f1
        bond-miimon 100
        bond-mode 802.3ad
        bond-xmit-hash-policy layer2+3

auto bond0.3004
iface bond0.3004 inet manual
        vlan-id 3004

auto bond1
iface bond1 inet manual
        bond-slaves enp33s0f0 enp1s0f1
        bond-miimon 100
        bond-mode 802.3ad

auto vmbr0
iface vmbr0 inet static
        address 172.20.4.104/24
        gateway 172.20.4.254
        bridge-ports bond0.3004
        bridge-stp off
        bridge-fd 0

auto vmbr1
iface vmbr1 inet static
        address 192.168.4.104/24
        bridge-ports bond1
        bridge-stp off
        bridge-fd 0

auto vmbr2
iface vmbr2 inet manual
        bridge-ports bond0
        bridge-stp off
        bridge-fd 0
        bridge-vlan-aware yes
        bridge-vids 2-4094
#VLAN aware bridge for all vms
```

After saving the updated `/etc/network/interfaces` configuration, reboot the server to ensure that the SFP modules are loaded properly.

Once you have the networking working, you should be able to ssh in as root and continue via a terminal.

### Configure PVE Free Repo

There is a bug in the interface that needs to be fixed if you need to do any more changes to the networking via the GUI. To install any updates or updated packages, you need to disable the enterprise repo and enable to open source one.

You will need to remove the repo file that configures the enterprise repo.

``` shell
root@proxmox:~# rm /etc/apt/sources.list.d/pve-enterprise.list
```

Create the file `/etc/apt/sources.list.d/pve-no-subscription.list` with the following contents:

``` ini
# PVE pve-no-subscription repository provided by proxmox.com,
# NOT recommended for production use
deb http://download.proxmox.com/debian/pve buster pve-no-subscription
```

### Update all of the packages to the latest release

There is a bug in the web interface for configuring network interfaces, so you will need to run a full update and reboot the host after configuring the networking.
Update the apt cache and install all updates.

``` bash
root@proxmox:~# apt update && apt -y upgrade
```

A new kernel is installed so a reboot would be suggested at this time.

### Configure Clustering

#### Creating a New Cluster

Login to the Proxmox web interface and under "Datacenter", go to the "Cluster" tab.  Click the "Create Cluster" button on the top of the page, give the cluster a name, and ensure that the link is using the correct interface.

This process should only have to be done on one cluster.

#### Joining an Existing Cluster

The easiest way to add the node to a cluster is via the web interface, taking an exert from the Proxmox manual

``` text
Join Node to Cluster via GUI

Login to the web interface on an existing cluster node. Under Datacenter -> Cluster, click the button Join Information at the top. Then, click on the button Copy Information. Alternatively, copy the string from the Information field manually.

Next, login to the web interface on the node you want to add. Under Datacenter -> Cluster, click on Join Cluster. Fill in the Information field with the Join Information text you copied earlier. Most settings required for joining the cluster will be filled out automatically. For security reasons, the cluster password has to be entered manually.
```

### Install and Configure CEPH

There are a few steps to configuring ceph, but they make it fairly easy. To install the software components you need, run the following command:

``` shell
root@proxmox-04:~# pveceph install -version nautilus
```

You will be prompted to confirm, and the installation will continue.

#### Initial Configuration

If this is the first node, you will need to create a configuration file.

``` shell
# One ONE NODE
pveceph init --network 172.20.4.104 --cluster-network 192.168.4.104/24
```

> If you are following along with these instructions, you will have already joined the ceph nodes together in a cluster before running pveceph init, so the base configuration will be copied over to all the other nodes.

#### Create Monitors

> The following commands should be run on all nodes.

Login to each ceph node and add a monitor (mon):

``` shell
root@proxmox:~# pveceph mon create
```

#### Create managers

Same goes for managers if you need to add additional ones:

``` shell
root@proxmox:~# pveceph mgr create
```

#### Zap Drives use in previous cluster

If you are using drives that were previously used in a cluster, you will need to zap the drive, you can use the following command for this

``` shell
for drives in {a,b,c,d,e,f,g,h,k,l}; do ceph-volume lvm zap /dev/sd$drives --destroy; done
```

#### Add OSD's to cluster

To add the OSDs, you can run the following command.  This should add all of the drives that are not currently in use as OSDs to ceph.

``` shell
root@proxmox:~# for drive in {a,b,c,d,e,f,g,h,k,l}; do pveceph osd create /dev/sd$drive; done
```

!NOTE:  While you can run this `for` loop on all ceph nodes simultaneously, there is a chance that there might be registration conflicts resulting in flapping OSDs, hung processes, and other general headaches.  It is advised to run the `for` loop to add OSDs on each server consecutively instead of simultaneously.

### Configure Ceph Pools

In order to configure ceph you must have a minimum of 3 nodes online for the default configuration.
For out deployment we utilize 3 separate pools for our storage needs. One is a CephFS so you will have 2 pools for it, a cephfs_data and a cephfs_metadata pool. The other two are for VM Storage and one for the PaaS team to use internally.

#### CephFS

To create the cephfs pool, run the following command on all of the nodes. This will create the Metadata service, we create one on each node incase of a failure of a host.

``` shell
pveceph mds create
```

Once the service is created on all nodes, on a single node you can create the filesystem

``` shell
# Run this on any ONE node.
pveceph fs create --pg_num 256 --add-storage
```

#### RBD Pools

To create the remaining pools, on a single node, we just need to run the following

``` shell
# Run this on any ONE node.
pveceph pool create vms --pg_num 256 --add_storages
pveceph pool create PaaS --pg_num 128 --add_storages
```
